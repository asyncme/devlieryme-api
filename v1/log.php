<? 

/**
	Method to write a log to database to save when entity is upgraded
*/
function WriteToAccountLog($companyId,$userId,$message){
	
	//Get timeStamp from function.php
	$time = GetCurrentTimeStamp();

	//Save log to describe what happen
	$data = array(
		"companyId" => $companyId,
		"time" => $time,
		"message" => $message,
		"userId" => $userId
		);

	$sql = "INSERT INTO accountLog (companyId,time,description,userId) VALUES(:companyId,:time,:message,:userId)";
	sbexeculteQueryWithData($sql,$data);
	
}

/**
	Method to save notification sent
*/
function WriteLog(){
	
	require 'lang.php';
	require "WSResponseCode.php";

	$message = $_POST['message'];
	$file = 'log.txt';
	// Open the file to get existing content
	$current = file_get_contents($file);
	// Write the contents back to the file
	$current .= "$message \n";
	file_put_contents($file, $current);

	$array = array(
			"status" => '1',
			"msgStatus" => 'success',
			"message" => "$logSuccess",
			"WSResponseCode" => "$WSCodeLogSuccess"
		);
	
	echo json_encode($array, JSON_PRETTY_PRINT);
}

/**
	Method to save notification sent
*/
function WriteToActionLog(){
	
	require 'lang.php';
	require "WSResponseCode.php";

	$data = array(
			"actionCode" => $_POST['actionCode'],
			"actionFrom" => $_POST['actionFrom'],
			"actionTo" => $_POST['actionTo'],
			"tableName" => $_POST['tableName'],
			"time" => GetCurrentTimeStamp(),
			"timezone" => $_POST['tz'],
			"note" => $_POST['note'],
			"companyId" => $_POST['companyId']
		);

	$sql = "INSERT INTO actionLog (actionCode,actionFrom,actionTo,tableName,time,timezone,note,companyId) 
			VALUES(:actionCode,:actionFrom,:actionTo,:tableName,:time,:timezone,:note,:companyId)";
	$query = sbexeculteQueryWithData($sql,$data);
	
	if ($query) {
	
		$array = array(
			"status" => '1',
			"msgStatus" => 'success',
			"message" => "$logSuccess",
			"WSResponseCode" => "$WSCodeLogSuccess"
		);		

	}else{
		
		$array = array(
			"status" => '0',
			"msgStatus" => 'fail',
			"message" => "$logFail",
			"WSResponseCode" => "$WSCodeLogFail"
		);

	}
	
	echo json_encode($array, JSON_PRETTY_PRINT);
}


/**
	Method to save notification sent
*/
function LogString($message){
	
	require 'lang.php';

	$file = 'log.txt';
	// Open the file to get existing content
	$current = file_get_contents($file);
	// Write the contents back to the file
	$current .= "$message \n";
	file_put_contents($file, $current);

}

?>