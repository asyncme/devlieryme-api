<?php

/** 
Method that will checkin user
*/
function ClockIn(){

	require 'lang.php';
	require "WSResponseCode.php";

	$credential = array(
					"personId" => $_POST['personId'],
					"token" => $_POST['token']
				);

	//Validate user access
	if (ValidateAccess($credential)) {

		$data = array(
			"time" => $_POST['ts'],
			"timezone" => $_POST['timezone'],
			"lat" => $_POST['lat'],
			"lng" => $_POST['lng'],
			"personId" => $_POST['personId'],
			"placeId" => $_POST['placeId'],
			"companyId" => $_POST['companyId'],
			"actionType" => 1
		);

		$sql = "INSERT INTO action (personId,placeId,time,timezone,lat,lng,companyId,actionType) 
				VALUES (:personId,:placeId,:time,:timezone,:lat,:lng,:companyId,:actionType)";
			
		//Get checkin id
		$actionId = sbexeculteQueryWithDataReturnId($sql,$data);

		if($actionId){
			
			$array = array(
				"status" => '1',
				"msgStatus" => 'success',
				"message" => "$msgActionClockInSuccess",
				"WSResponseCode" => "$WSCodeActionClockInSuccess",
				"actionId" => "$actionId"
			);	

		}else{

			$array = array(
				"status" => '0',
				"msgStatus" => 'fail',
				"message" => "$msgActionClockInFail",
				"WSResponseCode" => "$WSCodeActionClockInFail"
			);
		}
		
	}else{
		
		$array = array(
			"status" => '0',
			"msgStatus" => 'fail',
			"message" => "$msgLoginAccessDenied",
			"WSResponseCode" => "$WSCodeLoginAccessDenied"
		);
	
	}

	echo json_encode($array, JSON_PRETTY_PRINT);
}
/** 
Method that will checkin user
*/
function ClockOut(){

	require 'lang.php';
	require "WSResponseCode.php";

	$credential = array(
					"personId" => $_POST['personId'],
					"token" => $_POST['token']
				);

	//Validate user access
	if (ValidateAccess($credential)) {

		$data = array(
			"time" => $_POST['ts'],
			"timezone" => $_POST['timezone'],
			"lat" => $_POST['lat'],
			"lng" => $_POST['lng'],
			"personId" => $_POST['personId'],
			"placeId" => $_POST['placeId'],
			"companyId" => $_POST['companyId'],
			"actionType" => 2
		);

		$sql = "INSERT INTO action (personId,placeId,time,timezone,lat,lng,companyId,actionType) 
				VALUES (:personId,:placeId,:time,:timezone,:lat,:lng,:companyId,:actionType)";
			
		//Get checkin id
		$actionId = sbexeculteQueryWithDataReturnId($sql,$data);

		if($actionId){
			
			$array = array(
				"status" => '1',
				"msgStatus" => 'success',
				"message" => "$msgActionClockOutSuccess",
				"WSResponseCode" => "$WSCodeActionClockOutSuccess",
				"actionId" => "$actionId"
			);	

		}else{

			$array = array(
				"status" => '0',
				"msgStatus" => 'fail',
				"message" => "$msgActionClockOutFail",
				"WSResponseCode" => "$WSCodeActionClockOutFail"
			);
		}
		
	}else{
		
		$array = array(
			"status" => '0',
			"msgStatus" => 'fail',
			"message" => "$msgLoginAccessDenied",
			"WSResponseCode" => "$WSCodeLoginAccessDenied"
		);
	
	}

	echo json_encode($array, JSON_PRETTY_PRINT);
}

/**
	Method get user log.
	Limiting the quantity of data retrieve to 10.
*/
function GetPersonActions() {

	require 'lang.php';
	require "WSResponseCode.php";

	$credential = array(
					"personId" => $_POST['personId'],
					"token" => $_POST['token']
				);

	//Validate user access
	if (ValidateAccess($credential)) {
	
		$limit = $_POST['limit'];

		if (isset($_POST['actionId'])) {
			
			$actionId = $_POST['actionId'];

			if($actionId != 0){
			
				$sql = "SELECT *,action.time as actTime,action.actionId as actId,acn.time as acnTime,acn.actionId as acnId,acn.timezone as acnTimezone FROM action
						LEFT JOIN place ON action.placeId = place.placeId
						LEFT JOIN actionNote AS acn ON action.actionId = acn.actionId
						WHERE action.personId =  ".$credential['personId']." AND action.actionId < $actionId
						ORDER BY action.actionId DESC
						LIMIT 10";
			}else{
			
				$sql = "SELECT *,action.time as actTime,action.actionId as actId,acn.time as acnTime,acn.actionId as acnId,acn.timezone as acnTimezone FROM action
						LEFT JOIN place ON action.placeId = place.placeId
						LEFT JOIN actionNote AS acn ON action.actionId = acn.actionId
						WHERE action.personId =  ".$credential['personId']."
						ORDER BY action.actionId DESC
						LIMIT 10";				
			}
			
		}else{

			$sql = "SELECT *,action.time as actTime,action.actionId as actId,acn.time as acnTime,acn.actionId as acnId,acn.timezone as acnTimezone FROM action
						LEFT JOIN place ON action.placeId = place.placeId
						LEFT JOIN actionNote AS acn ON action.actionId = acn.actionId
						WHERE action.personId =  ".$credential['personId']."
						ORDER BY action.actionId DESC
						LIMIT 10";
		}

		$query = sbexeculteQuery($sql);
		$row = $query->rowCount();
		
		if($row != 0){

			$i = 0;
			$loop = array();

			while($fetch = $query->fetch()){
				
				$loop[$i]['actionId'] = $fetch->actId;
				$loop[$i]['personId'] = $fetch->personId;
				$loop[$i]['time'] = $fetch->actTime;
				$loop[$i]['timezone'] = $fetch->timezone;
				$loop[$i]['lat'] = $fetch->lat;
				$loop[$i]['lng'] = $fetch->lng;
				$loop[$i]['placeId'] = $fetch->placeId;
				$loop[$i]['placeName'] = $fetch->placeName;
				$loop[$i]['onRadius'] = $fetch->onRadius;
				$loop[$i]['actionType'] = $fetch->actionType;
				
				if(isset($fetch->acnId)){
					$loop[$i]['actionNoteId'] = $fetch->acnId;
					$loop[$i]['note'] = $fetch->note;
					$loop[$i]['noteTime'] = $fetch->acnTime;
					$loop[$i]['noteTimezone'] = $fetch->acnTimezone;
				}
				
				$i++;
			}
			
				$array = array(
				"status" => '1',
				"msgStatus" => 'success',
				"message" => "$msgLogUserSuccess",
				"WSResponseCode" => "$WSCodeLogUserSuccess",
				"actions" => $loop
			);

		}else{
			$array = array(
				"status" => '0',
				"msgStatus" => 'fail',
				"message" => "$msgLogUserNoLog",
				"WSResponseCode" => "$WSCodeLogNoLog"
			);
		}

		if (!$query) {
			$array = array(
				"status" => '0',
				"msgStatus" => 'fail',
				"message" => "$msgLogUserFail",
				"WSResponseCode" => "$WSCodeLogUserFail"
			);
		}

		
	}else{

		$array = array(
			"status" => '0',
			"msgStatus" => 'fail',
			"message" => "$msgLoginAccessDenied",
			"WSResponseCode" => "$WSCodeLoginAccessDenied"
		);
	}

	echo json_encode($array, JSON_PRETTY_PRINT);
}


/** 
	Fetch all place notes
*/
function GetActionNote(){
	
	require 'lang.php';
	require "WSResponseCode.php";

	$credential = array(
					"personId" => $_POST['personId'],
					"token" => $_POST['token']
				);

	//Validate user access
	if (ValidateAccess($credential)) {

		$placeId = $_POST['placeId'];
		
		$sql = "SELECT *,actionNote.time as actionNoteTime, actionNote.timezone as actionNoteTimezone FROM actionNote
		    	INNER JOIN checkin ON actionNote.actionId = checkin.actionId
		      	INNER JOIN site ON checkin.placeId = site.placeId
		      	INNER JOIN user ON checkin.personId = user.personId
		      	WHERE checkin.placeId = '$placeId' AND actionNote.active = 1 AND actionNote.isNote = 1
		      	ORDER BY actionNote.time DESC";
	
		$query = sbexeculteQuery($sql);
		$row = $query->rowCount();

		if ($row != 0) {

            $i = 0;
            $loop = array();

            while($fetch = $query->fetch()){

            	//actionNote information
                $loop[$i]['actionNoteId'] = $fetch->actionId;
                $loop[$i]['actionId'] = $fetch->actionId;
                $loop[$i]['note'] = $fetch->note;
                $loop[$i]['time'] = $fetch->actionNoteTime;
                $loop[$i]['timezone'] = $fetch->actionNoteTimezone;
                
                //Site information
                $arraySiteInfo = array();
                $arraySiteInfo['placeId'] = $fetch->placeId;
                $arraySiteInfo['siteName'] = $fetch->name;
				$loop[$i]['siteInfo'] = $arraySiteInfo;

                //User information
                $arrayUserInfo = array();
                $arrayUserInfo['personId'] = $fetch->personId;
                $arrayUserInfo['name'] = $fetch->firstName . ' ' . $fetch->lastName;
                $arrayUserInfo['email'] = $fetch->email;
                $arrayUserInfo['mobile'] = $fetch->mobile;
                $loop[$i]['userInfo'] = $arrayUserInfo;

                $i++;
            }

            $array = array(
                "status" => '1',
                "msgStatus" => 'success',
                "message" => "$msgSiteNoteSuccess",
                "WSResponseCode" => "$WSCodeSiteNoteSuccess",
                "siteNote" => $loop
            );

            echo json_encode($array, JSON_PRETTY_PRINT);
		}else{

			$array = array(
	            "status" => '0',
	            "msgStatus" => 'fail',
	            "message" => "$msgSiteNoteNoNote",
	            "WSResponseCode" => "$WSCodeSiteNoteNoNote"
        	);

			echo json_encode($array, JSON_PRETTY_PRINT);
		}

    }else{

        $array = array(
            "status" => '0',
            "msgStatus" => 'fail',
            "message" => "$msgLoginAccessDenied",
            "WSResponseCode" => "$WSCodeLoginAccessDenied"
        );

        echo json_encode($array, JSON_PRETTY_PRINT);
    }
}

/**
Save Action note
 */
function SaveActionNote(){

    require 'lang.php';
    require "WSResponseCode.php";

    $credential = array(
					"personId" => $_POST['personId'],
					"token" => $_POST['token']
				);

	//Validate user access
	if (ValidateAccess($credential)) {

		$data = array(
		    "actionId" => $_POST['actionId'],
		    "note" => $_POST['note'],
			"time" => $_POST['ts'],
			"timezone" => $_POST['tz']
		);

		$sqlExist = "SELECT * FROM actionNote WHERE actionId = ".$data['actionId']."";
		$queryExist = sbexeculteQuery($sqlExist);
        $row = $queryExist->rowCount();

        if ($row == 0) {
            $sql = "INSERT INTO actionNote (actionId,note,time,timezone) VALUES (:actionId,:note,:time,:timezone)";
        }else{
            $sql = "UPDATE actionNote SET note = :note, time = :time, timezone = :timezone WHERE actionId = :actionId";
        }

        $query = sbexeculteQueryWithData($sql,$data);

        if ($query) {

            $array = array(
                "status" => '1',
                "msgStatus" => 'success',
                "message" => "$msgActionNoteSaveSuccess",
                "WSResponseCode" => "$WSCodeActionNoteSaveSuccess"
            );

        }else{

            $array = array(
                "status" => '0',
                "msgStatus" => "fail",
                "message" => "$msgActionNoteSaveFail",
                "WSResponseCode" => "$WSCodeActionNoteSaveFail"
            );

        }

    }else{

        $array = array(
            "status" => '0',
            "msgStatus" => 'fail',
            "message" => "$msgLoginAccessDenied",
            "WSResponseCode" => "$WSCodeLoginAccessDenied"
        );
    }

    echo json_encode($array, JSON_PRETTY_PRINT);
}

?>
