
<?

function sbexeculteQuery($sql){

	require 'lang.php';
	require "WSResponseCode.php";

	try{
		
		$sbdb = SBConnection();
		$query = $sbdb->prepare($sql);
		
		if ($query->execute()) {
			
			$query->setFetchMode(PDO::FETCH_OBJ);
			return $query;	

		}

	}catch(PDOException $e){
		$array = array(
			"status" => '0',
			"msgStatus" => 'fail',
			"message" => $e->getMessage(),
			"WSResponseCode" => "$WSCodePDOError"
		);

		return $array;
	}

}

function sbexeculteQueryWithData($sql,$data){

	require 'lang.php';
	require "WSResponseCode.php";

	try{
		
		$sbdb = SBConnection();
		$query = $sbdb->prepare($sql);

		if ($query->execute($data)) {		
			
			$query->setFetchMode(PDO::FETCH_OBJ);
			return $query;

		}

	}catch(PDOException $e){

		$array = array(
			"status" => '0',
			"msgStatus" => 'fail',
			"message" => $e->getMessage(),
			"WSResponseCode" => "$WSCodePDOError"
		);

		return $array;
	}

}

function sbexeculteQueryWithDataReturnId($sql,$data){

	require 'lang.php';
	require "WSResponseCode.php";

	try{
		
		$sbdb = SBConnection();

		$query = $sbdb->prepare($sql);

		if ($query->execute($data)) {
			return $sbdb->lastInsertId();
		}
		
	}catch(PDOException $e){
		
		$array = array(
			"status" => '0',
			"msgStatus" => 'fail',
			"message" => $e->getMessage(),
			"WSResponseCode" => "$WSCodePDOError"
		);

		return $array;
	}

}

?>