<?php
/**
	Method user login
*/

function Login() {

	require 'lang.php';
	require "WSResponseCode.php";

	//$password = $_POST['password'];
	$password_encoded = encryptStr($_POST['password']);

	$data = array(
		"email" => $_POST['email']
	);

	$sql = "SELECT * FROM person WHERE email = :email";
	$query = sbexeculteQueryWithData($sql,$data);
	$row = $query->rowCount();

	if($row != 0){

		$fetch = $query->fetch();

		//if (password_verify($password, $fetch->password) == 1)
		if ($password_encoded === $fetch->password) {

			$sqlCompany = "	SELECT * FROM companyMap
											INNER JOIN company ON companyMap.companyId = company.companyId
											INNER JOIN person ON companyMap.personId = person.personId
											INNER JOIN companySetting ON companyMap.companyId = companySetting.companyId
											WHERE person.personId = $fetch->personId";
			$queryCompany = sbexeculteQuery($sqlCompany);
			$rowCompany = $queryCompany->rowCount();

			if ($rowCompany != 0) {

				$fetchData = $queryCompany->fetch();

				//Generate token with length 20
				$token = GenerateToken(20);

				//Update user token
				UpdateToken($token,$fetch->personId);

				$array = array(
					"status" => '1',
					"msgStatus" => 'success',
					"message" => "$msgLoginSuccess",
					"WSResponseCode" => "$WSCodeLoginSuccess",
					"personId" => $fetchData->personId,
					"token" => $token,
					"email" => $fetchData->email,
					"name" => $fetchData->name,
					"mobile" => $fetchData->mobile,
					"driver" => $fetchData->driver,
					"companyId" => $fetchData->companyId,
					"adminId" => $fetchData->adminId,
					"companyName" => $fetchData->companyName
				);

			}else{

				$array = array(
					"status" => '0',
					"msgStatus" => 'fail',
					"message" => "$msgLoginFail",
					"WSResponseCode" => "$WSCodeLoginFail"
				);

			}

		}else{

			$array = array(
				"status" => '0',
				"msgStatus" => 'fail',
				"message" => "$msgLoginFail",
				"WSResponseCode" => "$WSCodeLoginFail"
			);

		}

	}else{

		$array = array(
			"status" => '0',
			"msgStatus" => 'fail',
			"message" => "$msgLoginFail",
			"WSResponseCode" => "$WSCodeLoginFail"
		);

	}

	echo json_encode($array, JSON_PRETTY_PRINT);

}

/**
	Method to update device id
*/
function UpdateDevice() {

	require 'lang.php';
	require "WSResponseCode.php";

	$data = array(
		"personId" => $_POST['personId'],
		"deviceId" => $_POST['deviceId'],
		"deviceType" => $_POST['deviceType'],
		"time" => $_POST['dateModified'],
		"timezone" => $_POST['timezone'],
		"version" => $_POST['version']
	);

	//Check to see if necessary to udate password
	$sql = "SELECT * FROM tokenDevice WHERE personId = ".$data['personId']."";

	$query = sbexeculteQuery($sql);
	$row = $query->rowCount();

	//Check if the only token existent is equal to the one sent and update if necessary
	if ($row == 1) {

		$fetch = $query->fetch();

		if ($fetch->deviceId != $data['deviceId']) {

			$sqlDevice = "UPDATE tokenDevice SET deviceId = :deviceId, deviceType = :deviceType, dateModified = :time, timezone = :timezone, version = :version
						WHERE personId = :personId";
			$queryDevice = sbexeculteQueryWithData($sqlDevice,$data);

		}else{
			$queryDevice = TRUE;
		}

	//If more then one token, delete all and insert a new one
	}else if($row > 1){

		//Remove any tokenId that matchs with the one sent
		sbexeculteQuery("DELETE FROM tokenDevice WHERE personId = '$personId'");
		//Insert new token device to table
		$sqlDevice = "	INSERT INTO tokenDevice (personId,deviceId,deviceType,dateModified,timezone,version)
						VALUES (:personId,:deviceId,:deviceType,:time,:timezone,:version)";
		$queryDevice = sbexeculteQueryWithData($sqlDevice,$data);

	//If none, insert a new token
	}else{

		$sqlDevice = "	INSERT INTO tokenDevice (personId,deviceId,deviceType,dateModified,timezone,version)
						VALUES (:personId,:deviceId,:deviceType,:time,:timezone,:version)";
		$queryDevice = sbexeculteQueryWithData($sqlDevice,$data);
	}

	if($queryDevice){
		$array = array(
			"status" => '1',
			"msgStatus" => 'success',
			"message" => "$msgTokenDeviceUpdateSuccess",
			"WSResponseCode" => "$WSCodeTokenDeviceUpdateSuccess"
		);
	}else{
		$array = array(
			"status" => '0',
			"msgStatus" => 'fail',
			"message" => "$msgTokenDeviceUpdateFail",
			"WSResponseCode" => "$WSCodeTokenDeviceUpdateFail"
		);
	}

	echo json_encode($array, JSON_PRETTY_PRINT);

}

?>
