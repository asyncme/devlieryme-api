<?php

/**
IMPORT THE IMPORTANT PHP FILES
*/
require 'sbpdo.php';
require 'database/SBConnection.php';
require 'Slim/Slim.php';

require 'person.php';
require 'route.php';
require 'email.php';
require 'setting.php';
require 'uploadImg.php';
require 'functions.php';

\Slim\Slim::registerAutoloader();
$app = new \Slim\Slim();

/**
PERSON
*/
//user login
$app->post('/login/', 'Login');
//update device
$app->post('/updateDevice/', 'UpdateDevice');

/**
Route
*/
//get all routes for user
$app->post('/routes/', 'GetRouteByUserId');
//get all places for route
$app->post('/route/', 'GetRouteById');

/**
PASSWORD RESET
*/
//user forgot password
$app->post('/forgotPassword/', 'ForgotPassword');
$app->get('/resetPassword/:personId/:tokenReset', function($personId,$tokenReset){
    ResetPassword($personId, $tokenReset);
});

/**
UPLOAD
*/
//Save to actionLog table
$app->post('/imgUpload/', 'ImageUpload');

/**
TEMPORARY
*/
//Review user password
$app->post('/encode/', 'EncodePassword');


/**
EMPTY ACCESS BLOCK
*/
$app->get(
    '/ping',
    function () {
        $template = <<<EOT
<!DOCTYPE html>
    <html>
        <head>
            <title>Delivery WebService</title>
        </head>
        <body>
			<p>Access not allowed</p>
        </body>
    </html>
EOT;
        echo $template;
    }
);

$app->run();

?>
