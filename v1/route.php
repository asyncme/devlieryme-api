<?

/**
	Method that retrieve all the site based on entity id
*/
function GetRouteByUserId(){

	require 'lang.php';
	require "WSResponseCode.php";

	$credential = array(
					"personId" => $_POST['personId'],
					"token" => $_POST['token']
				);

	//Validate user access
	if (ValidateAccess($credential)) {

		$personId = $_POST['personId'];
		$time = GetCurrentTimeStamp();

		$sql = "SELECT * FROM route WHERE personId = $personId AND active = 1";

		$query = sbexeculteQuery($sql);
		$row = $query->rowCount();

		if($row != 0){

			$i = 0;
			$loop = array();

			while($fetch = $query->fetch()){

				$loop[$i]['routeId'] = $fetch->routeId;
				$loop[$i]['name'] = $fetch->name;
				$loop[$i]['time'] = $fetch->time;
				$loop[$i]['timezone'] = $fetch->timezone;
				$loop[$i]['companyId'] = $fetch->companyId;
				$loop[$i]['personId'] = $fetch->personId;
				$loop[$i]['qtdCustomers'] = getQtdCustomers($fetch->routeId);
				$i++;
			}

			$array = array(
				"status" => '1',
				"msgStatus" => 'success',
				"message" => "$msgSiteFetchSuccess",
				"WSResponseCode" => "$WSCodeRouteFetchSuccess",
				"routes" => $loop
			);

		}else{
			$array = array(
				"status" => '0',
				"msgStatus" => 'fail',
				"message" => "$msgSiteFetchFail",
				"WSResponseCode" => "$WSCodeRouteFetchFail"
			);
		}

	}else{

		$array = array(
			"status" => '0',
			"msgStatus" => 'fail',
			"message" => "$msgLoginAccessDenied",
			"WSResponseCode" => "$WSCodeLoginAccessDenied"
		);
	}

	echo json_encode($array, JSON_PRETTY_PRINT);
}

/**
	Method that retrieve all the site based on entity id
*/
function GetRouteById(){

	require 'lang.php';
	require "WSResponseCode.php";

	$credential = array(
					"personId" => $_POST['personId'],
					"token" => $_POST['token']
				);

	//Validate user access
	if (ValidateAccess($credential)) {

		$companyId = $_POST['companyId'];
		$routeId = $_POST['routeId'];
		$time = GetCurrentTimeStamp();

		$sql = "SELECT * FROM routeMap
						INNER JOIN customer ON routeMap.customerId = customer.customerId
						WHERE routeId = $routeId AND routeMap.active = 1";

		$query = sbexeculteQuery($sql);
		$row = $query->rowCount();

		if($row != 0){

			$i = 0;
			$loop = array();

			while($fetch = $query->fetch()){

				$loop[$i]['routeMapId'] = $fetch->routeMapId;
				$loop[$i]['routeId'] = $fetch->routeId;
				$loop[$i]['companyId'] = $fetch->companyId;
				$loop[$i]['customerId'] = $fetch->customerId;
				$loop[$i]['order'] = $fetch->order;
				$loop[$i]['name'] = $fetch->name;
				$loop[$i]['mobile'] = $fetch->mobile;
				$loop[$i]['note'] = $fetch->note;
				$loop[$i]['address'] = $fetch->address;
				$loop[$i]['lat'] = $fetch->lat;
				$loop[$i]['lng'] = $fetch->lng;

				$i++;
			}

			$array = array(
				"status" => '1',
				"msgStatus" => 'success',
				"message" => "$msgSiteFetchSuccess",
				"WSResponseCode" => "$WSCodeRouteDetailSuccess",
				"customers" => $loop
			);

		}else{
			$array = array(
				"status" => '0',
				"msgStatus" => 'fail',
				"message" => "$msgSiteFetchFail",
				"WSResponseCode" => "$WSCodeDetailFail"
			);
		}

	}else{

		$array = array(
			"status" => '0',
			"msgStatus" => 'fail',
			"message" => "$msgLoginAccessDenied",
			"WSResponseCode" => "$WSCodeLoginAccessDenied"
		);
	}

	echo json_encode($array, JSON_PRETTY_PRINT);
}

?>
