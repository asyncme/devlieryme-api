<?

/**
	///////////////////////////// TEAM FUNCTIONS /////////////////////////////
*/

function getQtdCustomers($routeId){

	$sql = "SELECT routeMapId FROM routeMap WHERE routeId = $routeId AND routeMap.active = 1";
	$query = sbexeculteQuery($sql);

	if ($query) {
		return $query->rowCount();
	}else{
		return 0;
	}

}

/**
	Method to get user last latitude and longitude, also check if actionTypeId different then 9
*/
function UserLastLocation($personId){

	$sql = "SELECT actionId,time,timezone,
			action.lat as latitude,action.lng as longitude,action.actionTypeId,
			place.lat as placeLat,place.lng as placeLng,place.placeId,
			usercompanyId,actionBy,batteryLevel,isPulse,name,personId
			FROM action
			INNER JOIN actionType ON action.actionTypeId = actionType.actionTypeId
			LEFT JOIN place ON action.placeId = place.placeId
			WHERE action.personId = '$personId' AND action.actionBy = '$personId'
			ORDER BY action.actionId
			DESC LIMIT 1";

	$query = sbexeculteQuery($sql);

	if ($query) {
		return $query->fetch();
	}else{
		return false;
	}

}

/**
	Method to get user last latitude and longitude, also check if actionTypeId different then 9
*/
function UserLastLocationByEntity($personId,$companyId){

	$sql = "SELECT actionId,time,timezone,
			action.lat as latitude,action.lng as longitude,action.actionTypeId,
			place.lat as placeLat,place.lng as placeLng,place.placeId,
			usercompanyId,actionBy,batteryLevel,isPulse,name,personId
			FROM action
			INNER JOIN actionType ON action.actionTypeId = actionType.actionTypeId
			LEFT JOIN place ON action.placeId = place.placeId
			WHERE action.personId = '$personId' AND action.usercompanyId = '$companyId' AND action.actionBy = '$personId'
			ORDER BY action.actionId
			DESC LIMIT 1";

	$query = sbexeculteQuery($sql);

	if ($query) {
		return $query->fetch();
	}else{
		return false;
	}

}

/**
	Method to get who user is following.
*/
function GetFollowing($personId){

	$sql = "SELECT *,reportTo.active AS request FROM reportTo
			INNER JOIN user on reportTo.leaderId = user.personId
			WHERE reportTo.followerId = '$personId'
			AND user.active = 1
			AND (reportTo.active = 1 OR reportTo.active = 2)";

	$query = sbexeculteQuery($sql);
	return $query;
}

/**
	Method to get who is following user.
*/
function GetFollowers($personId){

	$sql = "SELECT *,reportTo.active AS request FROM reportTo
			INNER JOIN user on reportTo.followerId = user.personId
			WHERE reportTo.leaderId = '$personId'
			AND user.active = 1
			AND (reportTo.active = 1 OR reportTo.active = 2)";

	$query = sbexeculteQuery($sql);
	return $query;

}



/**
	///////////////////////////// place FUNCTIONS /////////////////////////////
*/

/**
	Method to check the quantity of users joined the entity
	If quantity of allowed user is less than the quantity joined return true, otherwise return false.
*/
function EntityplaceLimit($companyId,$allowplace){

	$sql = "SELECT * FROM place WHERE companyId = '$companyId' AND active = 1";
	$query = sbexeculteQuery($sql);
	$row = $query->rowCount();

	if ($row <= $allowplace) {
		return true;
	}else{
		return false;
	}
}

/**
	Methdd to get the length the user stayed on the place in the last 30 days
*/
function getUserLengthForplace($action){

	//Checkout
	$sqlCheckout = "	SELECT * FROM action
						WHERE placeId = '$action->placeId' AND personId = '$action->personId' AND ( actionTypeId = 3 || actionTypeId = 14 ) AND actionId > '$action->actionId'
						LIMIT 1";
	$queryCheckout = sbexeculteQuery($sqlCheckout);
	$row = $queryCheckout->rowCount();

	if ($row) {

		$fetchCheckout = $queryCheckout->fetch();

		return $fetchCheckout->time - $action->time;

	}else{
		return GetCurrentTimeStamp() - $action->time;
	}

}

/**
	///////////////////////////// USER FUNCTIONS /////////////////////////////
*/

/**
Method to create new password
*/
function createPassword($password){
	//return password_hash($password, PASSWORD_BCRYPT);
	return encryptStr($password);
}

/**
Method to create new password
*/
function EncodePassword(){
	//echo "hashed: " . password_hash($_POST['password'], PASSWORD_BCRYPT) . "\n\r";
	//echo "hashed: " . encryptStr($_POST['password']);
	$password_encoded = encryptStr($_POST['password']);
	echo "password: " . $password_encoded;
}

function encryptStr($str){

  $key = 'guess the key mate if you can';
  $encrypted = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $str, MCRYPT_MODE_CBC, md5(md5($key))));
  return $encrypted;

}

/**
	///////////////////////////// ENTITY FUNCTIONS /////////////////////////////
*/

/**
	Method to check the quantity of users joined the entity
	If quantity of allowed user is less than the quantity joined return true, otherwise return false.
*/
function EntityUserLimit($companyId,$allowUser,$timestamp){

	$sql = "SELECT * FROM entityMap
			WHERE companyId = '$companyId' AND (accessEnd > $timestamp OR accessEnd IS NULL) ";

	$query = sbexeculteQuery($sql);
	$row = $query->rowCount();

	if ($row <= $allowUser) {
		return true;
	}else{
		return false;
	}
}

/**
	Method to upgrade entity to next level when the quantity of user goes above the package quantity.
	It checks if the entity is auto upgrade before calling this method
*/
function UpgradeEntity($companyId,$personId,$pkg){

	sbexeculteQuery("UPDATE setting SET packageId = '$pkg' WHERE companyId = '$companyId'");

}

/**
	///////////////////////////// GENERAL FUNCTIONS /////////////////////////////
*/

/**
	Method return current timeStamp
*/
function GetCurrentTimeStamp(){
	$date = new DateTime();
	$timestamp = $date->getTimestamp();
	return $timestamp;
}

/**
	Method will list all the users involved with the notification reference
*/
function GetUserInfo($personId){

	$sql = "SELECT * FROM user
			LEFT JOIN tokenDevice ON user.personId = tokenDevice.personId
			WHERE user.personId = '$personId'";

	$query = sbexeculteQuery($sql);
	return $query->fetch();
}


/**
	Save note to actionNote
*/
function AddactionNote($actionId,$note,$time,$timezone){

	sbexeculteQuery("INSERT INTO actionNote (actionId,note,time,timezone) VALUES ($actionId,'$note',$time,'$timezone')");

}

/**
	Method to get user last action
*/
function personLastLocation($personId){

	$sql = "SELECT * FROM action
			INNER JOIN place ON action.placeId = place.placeId
			WHERE action.personId = '$personId' ORDER BY action.actionId DESC LIMIT 1";
	$query = sbexeculteQuery($sql);
	$row = $query->rowCount();

	if ($row) {
		return $query->fetch();
	}else{
		return false;
	}

}
?>
