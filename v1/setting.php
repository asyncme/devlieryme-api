<?

/** 
	Method to generate token for user and update the database
*/
function generateToken($length) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, strlen($characters) - 1)];
    }

    return $randomString;
}

/**
	Method to update user token
*/
function UpdateToken($token,$personId){

	//Remove any entry of api access for user on database
	sbexeculteQuery("DELETE FROM token WHERE personId = $personId");	
	sbexeculteQuery("INSERT INTO token (personId,token) VALUES ($personId,'$token')");
}

/**
	Method to Validate user token
*/
function ValidateAccess($credentials){

	$sql = "SELECT * FROM token WHERE personId = :personId and token = :token";
	$query = sbexeculteQueryWithData($sql,$credentials);
	$exist = $query->rowCount();
	
	if ($exist != 0) {
		return true;
	}else{
		return false;
	}
}

?>
