<?

/**
VALIDATE ACCESS
*/
$msgLoginAccessDenied = "Access Denied";

/**
LOGIN
*/
//Login to app
$msgLoginSuccess = "Welcome to Delivery";
$msgLoginFail = "Email and password don't match";
$msgLoginNotActive = "Your account is not active, please contact Clockme team";
$msgLoginForgotPassword = "A new temporary password has been sent to your registered email address";
$msgLoginInvalidEmail = "Email not found";

/**
PERSON
*/
//Register
$msgUserRegisterSucess = "User saved";
$msgUserRegisterFail = "Error occur, user not saved";
$msgUserRegisterExistent = "Email already registered";
//update user
$msgUserUpdateSuccess = "Your information was updated successfully";
$msgUserUpdateFail = "Error occur, update failed";
$msgUserUpdatePasswordSuccess = "Your password was updated successfully";
$msgUserUpdatePasswordFail = "Error occur, current password incorrect";
//user status
$msgUserStatusSuccess = "User status retrieved successfully";
$msgUserStatusFail = "Error occur, user status retrieve failed";
//update device token
$msgTokenDeviceUpdateSuccess = "Device token updated";
$msgTokenDeviceUpdateFail = "Error occur, device token not updated";
//update version
$msgUserVersionUpdateSuccess = "Device version updated";
$msgUserVersionUpdateFail = "Error occur, device version not updated";

/**
PLACE
*/
//Fetch site
$msgSiteFetchSuccess = "Site retrieve successfully";
$msgSiteFetchFail = "No site available";
//Site log
$msgSiteLogFetchSuccess = "Site log retrieve successfully";
$msgSiteLogFetchFail = "Error occur, site log not retrieved";
$msgSiteLogFetchNoLog = "No site log available";
//Site note
$msgActionNoteSuccess = "Action Note fetch successfully";
$msgActionNoteFail = "Error occur, Action Note fetch failed";
$msgActionNoteNoNote = "No Action Note available";
//Site note
$msgActionNoteSaveSuccess = "Action Note saved successfully";
$msgActionNoteSaveFail = "Error occur, Action Note not saved";

/**
ACTION
*/
//checkin
$msgActionClockInSuccess = "Check-in successfully";
$msgActionClockInFail = "Check-in failed";
//checkout
$msgActionClockOutSuccess = "Check-out successfully";
$msgActionClockOutFail = "Check-out failed";

/**
MY TEAM
*/
//All My team
$msgMyTeamSuccess = "Team retrieve successfully";
$msgMyTeamFail = "Error occur, retrieve failed";
$msgMyTeamNoTeam = "The company don't have person, please add them";
//Team leader by entity
$msgTeamLeaderFetchSuccess = "Team leader retrieve successfully";
$msgMyTeamNoLeader = "No Team leader set for this entity";
//My team leader
$msgMyTeamLeaderFetchSuccess = "My Team leader retrieve successfully";
$msgMyTeamLeaderNoLeader = "You have no team leader set";

/**
GET USER LOG
*/
$msgLogUserSuccess = "Log retrieve successfully";
$msgLogUserFail = "Error occur, retrieve failed";
$msgLogUserNoLog = "No log available";

/**
UPLOAD
*/

$msgImageSuccess = "File uploaded successfully";
$msgImageFail = "File not uploaded";
$msgImageInvalid = "Invalid file format";

/**
LOG MESSAGES
*/
$logSuccess = "Log saved successfully";
$logFail = "Error occur, log not saved";
$logEntityUpdateByUser = "Entity reached the maximum user capacity and was updated to next package.";
$logEntityUpdateBySite = "Entity reached the maximum site capacity and was updated to next package.";

?>
