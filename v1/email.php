<?php

/**
	Method when user forgot password
*/
function ForgotPassword() {

	require_once 'lang.php';
	require_once "WSResponseCode.php";

	$email = $_POST['email'];

	$query = sbexeculteQuery("SELECT * FROM person WHERE email = '$email'");
	$exist = $query->rowCount();

	if($exist != 0){

		$fetch = $query->fetch();
		$tokenReset = randomString(20);

		$dataToken = array(
			"personId" => $fetch->personId,
			"token" => $tokenReset
			);

		$sqlForget = "INSERT INTO passwordRecovery (personId,token) VALUES (:personId,:token)";
		$queryForget = sbexeculteQueryWithData($sqlForget,$dataToken);

		//Send email to user
		$subject = "Password reset request";
		$message = "<p>Hi $fetch->name, you have requested a new password, please click <a href='http://deliverymeapi.asyncme.com/v1/resetPassword/$fetch->personId/$tokenReset'>here</a> to have a new password send to your email<p/>";
		$message .= "<p>Regards,<br/> DeliveryMe Team <br/></p>";

		$msg = createEmail($message);
		$headers = getEmailHeaders();
		$sent = mail($email, $subject, $msg, $headers);

		$array = array(
			"status" => "1",
			"msgStatus" => 'success',
			"message" => "$msgLoginForgotPassword",
			"WSResponseCode" => "$WSCodeLoginForgotPassword"
		);

	}else{
		$array = array(
			"status" => "0",
			"msgStatus" => 'fail',
			"message" => "$msgLoginInvalidEmail",
			"WSResponseCode" => "$WSCodeLoginInvalidEmail"
		);
	}

	echo json_encode($array, JSON_PRETTY_PRINT);
}

/**
	Method when user forgot password
*/
function ResetPassword($personId,$tokenReset) {

	require_once 'lang.php';
	require_once "WSResponseCode.php";

	$query = sbexeculteQuery("SELECT * FROM passwordRecovery
														INNER JOIN person ON passwordRecovery.personId = person.personId
														WHERE passwordRecovery.personId = '$personId' AND token = '$tokenReset'
														ORDER BY passwordRecoveryId LIMIT 1");
	$exist = $query->rowCount();

	if($exist != 0){

		$fetch = $query->fetch();

		//Update forgetPassword
		$queryUpdate = sbexeculteQuery("DELETE FROM passwordRecovery WHERE passwordRecoveryId = '$fetch->passwordRecoveryId'");

		//Create a new 6 digits password
		$decriptedPass = randomString(6);

		//Hash that password to save to db
		$encryptPass = createPassword($decriptedPass);

		//Update user password in db
		$query = sbexeculteQuery("UPDATE person SET password = '$encryptPass' WHERE personId = '$personId'");

		//Send email to user
		$subject = "Password reset confirmation";
		$message = "<p>Hi $fetch->name, please use the following as a temporary password and change it when you next log in: <b>$decriptedPass.</b> <p/>";
		$message .= "<p>Regards,<br/> DeliveryMe Team <br/></p>";

		$msg = createEmail($message);
		$headers = getEmailHeaders();
		$sent = mail($fetch->email, $subject, $msg, $headers);

		echo "Password reset successfuly, please check your email.";

	}else{
		echo "Invalid token or token already used, please try reseting your password again";
	}
}


function randomString($len){

  	$key = "";
  	//$keys = array_merge(range(0,9), range("a", "z"), range("A", "Z"));
 	$keys = array_merge(range(0,9), range("a", "z"));

	for($i = 0; $i < $len; $i++){
		$key .= $keys[array_rand($keys)];
  	}

	return $key;
}

/**
	Method when person forgot password
*/
function SendLogToSupport() {

	require_once 'lang.php';
	require_once "WSResponseCode.php";

	$credential = array(
					"personId" => $_POST['personId'],
					"token" => $_POST['token']
				);

	//Validate person access
	if (ValidateAccess($credential)) {

		$email = "admin@asyncme.com";

		//Send email to person
		$subject = $_POST['personName'] . " app log";
		$message = $_POST['log'] . " \n\r";
		$message .= "personId: " . $credential['personId'] . " \n\r";
		$message .= "DeliveryMe Team \n\r";

		//Default header value
		$headers = 	'From:noreply@asyncme.com' . "\r\n" .
					'Reply-To: admin@asyncme.com' . "\r\n" .
					'X-Mailer: PHP/' . phpversion();

		mail($email,$subject,$message,$headers);

	}
}


function createEmail($txt)
	{
		$imgSrc   = 'http://asyncme.com/deliveryme/public/img/logo_clear_inverted.png'; // Change image src to your site specific settings
		$imgDesc  = 'DeliverMe logo'; // Change Alt tag/image Description to your site specific settings
		$imgTitle = 'DeliverMe Logo'; // Change Alt Title tag/image title to your site specific settings
		$alertonDesc  = 'DeliverMe logo';
		$alertonTitle = 'DeliverMe Logo';


			$msg = '<!DOCTYPE html>
					<html lang="en" class="loginHtml">
					<head>
						<meta charset="utf-8">
						<meta http-equiv="X-UA-Compatible" content="IE=edge">
						<meta name="viewport" content="width=device-width, initial-scale=1">
						<title>DeliverMe</title>
					</head>
					<body style="background-color:#F5F5F5">
					<div style="width: 100%; float: left; clear: both; text-align: center;">
						<div style="margin: 0 auto;">
							<div style="width:50%; height: 100%; min-height:100%; margin-top: 80px; border: 2px solid #428bca; border-radius: 15px; display: block; background-color: #fff;">

								<div style="margin: 0 auto;">
									<div style="width: 60%; margin: 0 20%;">
										<img src="'.$imgSrc.'" style="width:120px;">
									</div>
								</div>
								<div style="width:90%; height: 100%; float: center; text-align: left; margin: 20px 10px 10px 20px;">';
									$msg .= $txt;
									$msg .= '<br><br>
								</div>
							</div>
						</div><!-- end .row -->
					</div><!-- end .containter -->
					</body>
					</html>';

		return $msg;
	}

	/**
	 * Get the email headers
	 */
	function getEmailHeaders()
	{
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
		$headers .= 'From: "DeliveryMe" <noreply@asyncme.com>' . "\r\n";
		return $headers;
	}

?>
