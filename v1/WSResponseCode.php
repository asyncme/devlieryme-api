<?

/**
VALIDATE ACCESS
*/
$WSCodeLoginAccessDenied = 500;
$WSCodePDOError = 501;

/**
LOGIN
*/
$WSCodeLoginSuccess = 100;
$WSCodeLoginFail = 101;
$WSCodeLoginNotActive = 102;
$WSCodeLoginForgotPassword = 103;
$WSCodeLoginInvalidEmail = 104;

/**
PERSON
*/
$WSCodeUserRegisterSuccess = 110;
$WSCodeUserRegisterFail = 111;
$WSCodeUserRegisterExistent = 112;
$WSCodeUserUpdateSuccess = 113;
$WSCodeUserUpdateFail = 114;
$WSCodeUserUpdatePasswordSuccess = 115;
$WSCodeUserUpdatePasswordFail = 116;
$WSCodeUserStatusSuccess = 117;
$WSCodeUserStatusFail = 118;

/**
PLACE
*/
$WSCodeRouteFetchSuccess = 130;
$WSCodeRouteFetchFail = 131;
$WSCodeRouteDetailSuccess = 132;
$WSCodeDetailFail = 133;

/**
GET USER LOG
*/
$WSCodeLogUserSuccess = 210;
$WSCodeLogUserFail = 211;
$WSCodeLogNoLog = 212;

/**
UPLOAD
*/
$WSCodeImageSuccess = 250;
$WSCodeImageFail = 251;
$WSCodeImageInvalid = 252;

/**
LOG MESSAGES
*/
$WSCodeLogSuccess = 260;
$WSCodeLogFail = 261;
$WSCodeLogEntityUpdateByUser = 262;
$WSCodeLogEntityUpdateBySite = 264;

/**
DEVICE
*/
$WSCodeTokenDeviceUpdateSuccess = 350;
$WSCodeTokenDeviceUpdateFail = 351;
$WSCodeUserVersionUpdateSuccess = 352;
$WSCodeUserVersionUpdateFail = 353;

?>
